require('dotenv').config(); //For get environment variable
///const { keybaseMsg, keybaseCreateSubteam, keybaseAddMember, keybaseCreateChannel} = require('./libs/keybase');
const clear = require('clear');
const figlet = require('figlet');
var dayjs = require('dayjs');

const User = require("./models/user");
const Reg = require("./models/reg");
const Command = require("./models/command");

clear();

console.log( figlet.textSync('Kretable cron service', { horizontalLayout: 'full' }));


const InitiateMongoServer = require("./libs/db");
// Initiate Mongo Server
InitiateMongoServer();

const Bot = require('keybase-bot');
const bot = new Bot();

const giphyRandom = require("giphy-random");


async function main() {

  const username = process.env.keybase_user;
  const paperkey = process.env.keybase_paper;

  try {
    await bot.init(username, paperkey);

    const onMessage = async message => {
      if (message.content.type === "text") {
         let query = await Reg.findOne({username: message.sender.username});

         if (query){
           var find = message.content.text.body.match(/(?!\d\.\s?)\w+/g);
           let validCommand = await Command.findOne({
             $and: [
               { command:  find[0] },
               { role: query.role }
            ]
            });
           if (validCommand){
             switch (validCommand.state){
               case 1:
                 // 'Uj' command
                 if (query.states.sentRequest == false){
                   bot.chat.send(message.conversationId, { body:
                     'Sajnos most nem tudom még magamtól eldönteni, hogy melyik iskolába jársz :pensive:.\n' +
                     'Kérlek a .jelentkezes opcióval add meg nekem a nevedet és osztályodat, hogy @d3v_ felkereshessen\n'+
                     'Mutatok neked egy példát a kommanddal kapcsolatban:\n' +
                     '.jelentkezes Példa Máté 9.D'
                   });
                 }
                break;
               case 2:
                 if (query.states.sentRequest == false){
                   bot.chat.send({name: 'd3v_', topicType: 'chat'},{body:
                     'Hi!\n' +
                     'Jelentkezett egy új diák!\n\n' +
                     'Beküldött adatok: ' + message.content.text.body.replace('.jelentkezes','') +
                     '\nKeybase account: ' + message.sender.username +
                     '\nDatabase ID: ' + query._id +
                     '\n\n@kretable ' + dayjs().format('YYYY-MM-DD HH:mm')
                   });
                   query.states.sentRequest = true;
                   await query.save();
                   bot.chat.send(message.conversationId, { body:
                     'Köszönöm a jelentkezésed, hamarosan felkeresünk!\n'+
                     'Addig is további szép napot!'
                   });
                 } else {
                   bot.chat.send(message.conversationId, { body:
                     'Értékelem, a próbálkozásod, de már elküldtem az adataidat!\n'+
                     'Hamarosan jelentkezünk :wink:!'
                   });
                 }
                break;
               case 3:
                var userId = (message.content.text.body.replace('.validate','')).replace(/\s/g, '');
                let validate = await Reg.findOne({_id: userId});
                validate.states.validated = true;
                await validate.save();

                bot.chat.send({name: validate.username, topicType: 'chat'},{body:
                  'Kedves' + validate.username + '!\n' +
                  'Sikeresen hitelesítettük a személyazonosságodat!\n\n' +
                  ':warning:  Ahhoz, hogy hozzá tudjak férni az órarendedhez, szükségem lenne a belépési adataidra.:warning: \n' +
                  'Felhívnám a figyelmedet, hogy nem tárolok jelszót és felhasználónevet, de hozzáférési kulcsok igényléséhez szügségem van rá!\n' +
                  ':warning: Amennyiben beleeggyezel, hogy tárolhassam az access_token és a refresh_token-t kérlek haladj tovább a regisztrációval! :warning:\n\n' +
                  'Az alábbi kommandok állnak rendelkezésre:\n'+
                  '.felhasznalonev\n' +
                  '.jelszo\n' +
                  '.osztaly\n' +
                  '\n\n@kretable ' + dayjs().format('YYYY-MM-DD HH:mm')
                });

                break;
               case 4:
                console.log('block');
                break;
             }
           }
           else {
             var gif = await giphyRandom(process.env.giphy_api, { tag: "confused" });
             bot.chat.send(message.conversationId, { body:
               'Megmondom őszintén, erre nem tudok mit reagálni, max csak ezt:\n' +
               gif.data.image_mp4_url
             });
           }
         } else {
           bot.chat.send(message.conversationId, {body:
             'Kedves ' + message.sender.username + '!\n' +
             '@kretable-bot vagyok, vagy hosszabban Kréta timetable bot :robot_face: .\n' +
             'A legfőbb feladatom, hogy minden reggel kiküldjem a diákoknak az aznapi órarendjüket és ha változás történt benne, akkor azt is jelezzem.\n' +
             'Tervezőm @d3v_, aki azóta is folyamatosan új funkciókat épít belém :grinning:.\n\n' +
             'Jelenleg még nem tudom kijelenteni magamról, hogy 100%-os a működésem, de dolgozom rajta...\n\n' +
             'Amennyiben szeretnéd kipróbálni a funkciómat kérlek bizonyosodj meg róla, hogy az osztályban még nincsen senki regisztrálva!\n\n'+
             'Ha készen vagy, akkor kezdhetjük :smile:!\n' +
             'Ha még nincsen osztály csoportod akkor gépeld be, hogy \'.uj\'\n'
           });
           reg = new Reg({
             username: message.sender.username,
             role: "guest",
             states:{
               welcomeMessage: true,
               validated: false,
               sentRequest: false
             }
           });

           await reg.save();
         }

      }

    }
    console.log(`Listening for all channels...`);
    await bot.chat.watchAllChannelsForNewMessages(onMessage)

  } catch (error) {
    console.error(error);
  }
}

async function shutDown() {
  await bot.deinit();
  process.exit();
}

process.on("SIGINT", shutDown);
process.on("SIGTERM", shutDown);

main();
