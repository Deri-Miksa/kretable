const mongoose = require("mongoose");

const CommandSchema = mongoose.Schema({

  "command": {
    "type": "String"
  },
  "role": {
    "type": "String"
  },
  "state": {
    "type": "Number"
  }

});
module.exports = mongoose.model("command", CommandSchema);
