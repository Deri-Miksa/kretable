const mongoose = require("mongoose");

const RegSchema = mongoose.Schema({
  "username": {
    "type": "String"
  },
  "role": {
    "type": "String"
  },
  "states": {
    "welcomeMessage": {
      "type": "Boolean"
    },
    "validated": {
      "type": "Boolean"
    },
    "sentRequest": {
      "type": "Boolean"
    }
  }

});
module.exports = mongoose.model("reg", RegSchema);
