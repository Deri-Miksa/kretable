const mongoose = require("mongoose");

const UserSchema = mongoose.Schema({

  "student": {
    "type": "String"
  },
  "settings": {
    "team": {
      "type": "String"
    },
    "subteam": {
      "type": "String"
    },
    "channel": {
      "type": "String"
    },
    "institute_code": {
      "type": "String"
    },
    "tokens": {
      "access_token": {
        "type": "String"
      },
      "token_type": {
        "type": "String"
      },
      "expires_at": {
        "type": "Date"
      },
      "refresh_token": {
        "type": "String"
      }
    },
    "others": {
      "user_agent": {
        "type": "String"
      },
      "mobile_api_url": {
        "type": "String"
      },
      "api_base_url": {
        "type": "String"
      }
    }
  },
  "created_at": {
    "type": "Date"
  }

});

// export model user with UserSchema
module.exports = mongoose.model("user", UserSchema);
