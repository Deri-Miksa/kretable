const mongoose = require("mongoose");
var dayjs = require('dayjs');
// Replace this with your MONGOURI.
const MONGOURI = process.env.DATABASE_URL;

const InitiateMongoServer = async () => {
  try {
    await mongoose.connect(MONGOURI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false
    });
    console.log("\n [T] Date:" , dayjs().format());
    console.log(" [!] MSG: ", "Connected to MongoDB !!");
    console.log(" ---");
  } catch (e) {
    console.log(e);
    throw e;
  }
  //
};

module.exports = InitiateMongoServer;
