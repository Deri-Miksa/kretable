require('dotenv').config() //Read credentials from .env
const Bot = require('keybase-bot');

const username = process.env.keybase_user      // put a real username here
const paperkey = process.env.keybase_paper // put a real paperkey here
const bot = new Bot();


module.exports.notify = async event => {
  try {
    if (bot._initStatus !== 'initialized') {
      await bot.init(username, paperkey, {verbose: true});
      console.log("Bot initialized");
    }
  } catch (error) {
    console.error(error)
  }
}

exports.keybaseMsg = async (team, channel, msg) => {
  try {
    await bot.init(username, paperkey, {verbose: true})
    const config = {
      name: team,
      members_type: 'team',
      topic_name: channel
    }
    const message = {
      body: msg,
    }
    await bot.chat.send(config, message)
  } catch (error) {
    console.error("Keybase user notify falled:", error)
  } finally {
    await bot.deinit()
  }
}

exports.keybaseCreateSubteam = async (team) => {
  try {
    await bot.init(username, paperkey, {verbose: true})
    var config = {
      team: team
    }
    await bot.team.create(config);
  } catch (error) {
    console.error("Keybase user notify falled:", error)
  } finally {
    await bot.deinit()
  }
}

exports.keybaseAddMember = async (team, username, role) => {
  try {
    await bot.init(username, paperkey, {verbose: true})
    var config = {
      team: team,
      usernames: [
        {
          username: username,
          role: role
        }
      ]
    }
    await bot.team.addMembers(config);
  } catch (error) {
    console.error("Keybase user notify falled:", error)
  } finally {
    await bot.deinit()
  }
}

exports.keybaseCreateChannel = async (team, channel) => {
  try {
    await bot.init(username, paperkey, {verbose: true})
    var config = {
      team: team,
      channel: channel
    }
    await bot.chat.createChannel(config);
  } catch (error) {
    console.error("Keybase user notify falled:", error)
  } finally {
    await bot.deinit()
  }
}
