const mongoose = require("mongoose");

// Replace this with your MONGOURI.
const MONGOURI = process.env.DATABASE_URL;

const InitiateMongoServer = async () => {
  try {
    await mongoose.connect(MONGOURI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false
    });
  } catch (e) {
    console.log(e);
    throw e;
  }
  //
};

module.exports = InitiateMongoServer;
