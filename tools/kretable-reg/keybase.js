require('dotenv').config() //Read credentials from .env
const Bot = require('keybase-bot');

exports.keybaseMsg = async (msg) => {
  const bot = new Bot()
  try {
    const username = process.env.keybase_user      // put a real username here
    const paperkey = process.env.keybase_paper // put a real paperkey here
    await bot.init(username, paperkey, {verbose: true})
    const channel = {
      name: 'deri_miksa',
      members_type: 'team',
      topic_name: 'timetable'
    }
    const message = {
      body: msg,
    }
    await bot.chat.send(channel, message)
  } catch (error) {
    console.error("Keybase user notify falled:", error)
  } finally {
    await bot.deinit()
  }
}
//{"method": "send", "params": {"options": {"channel": {"name": "you,them"}, "message": {"body": "is it cold today?"}}}}

exports.keybaseCreateSubteam = async (team) => {
  const bot = new Bot()
  try {
    const username = process.env.keybase_user      // put a real username here
    const paperkey = process.env.keybase_paper // put a real paperkey here
    await bot.init(username, paperkey, {verbose: true})
    var config = {
      team: team
    }
    await bot.team.create(config);
  } catch (error) {
    console.error("Keybase user notify falled:", error)
  } finally {
    await bot.deinit()
  }
}

exports.keybaseAddMember = async (team, username, role) => {
  const bot = new Bot()
  try {
    const username = process.env.keybase_user      // put a real username here
    const paperkey = process.env.keybase_paper // put a real paperkey here
    await bot.init(username, paperkey, {verbose: true})
    var config = {
      team: team,
      usernames: [
        {
          username: username,
          role: role
        }
      ]
    }
    await bot.team.addMembers(config);
  } catch (error) {
    console.error("Keybase user notify falled:", error)
  } finally {
    await bot.deinit()
  }
}

exports.keybaseCreateChannel = async (team, channel) => {
  const bot = new Bot()
  try {
    const username = process.env.keybase_user      // put a real username here
    const paperkey = process.env.keybase_paper // put a real paperkey here
    await bot.init(username, paperkey, {verbose: true})
    var config = {
      team: team,
      channel: channel
    }
    await bot.chat.createChannel(config);
  } catch (error) {
    console.error("Keybase user notify falled:", error)
  } finally {
    await bot.deinit()
  }
}

// {"method": "create-team", "params": {"options": {"team": "deri_miksa."}}}

// {"method": "add-members", "params": {"options": {"team": "deri.miksa", "usernames": [{"username": "d3v_", "role": "owner"}]}}}
