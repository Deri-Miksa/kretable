const inquirer = require('inquirer');

module.exports = {
  askKretaCredetials: () => {
    const questions = [
      {
        name: 'student',
        type: 'input',
        message: 'Enter your real name:',
        validate: function( value ) {
          if (value.length) {
            return true;
          } else {
            return 'Please enter valid name.';
          }
        }
      },
      {
        name: 'team',
        type: 'checkbox',
        message: 'Select your team:',
        choices: [
          'deri_miksa.osztalyok', 'deri_miksa'
        ],
        default: ['deri_miksa.osztalyok'],
        validate: function(value) {
          if (value.length) {
            return true;
          } else {
            return 'Please enter valid institute code.';
          }
        }
      },
      {
        name: 'subteam',
        type: 'input',
        message: 'Enter the new subteam:',
        validate: function( value ) {
          if (value.length) {
            return true;
          } else {
            return 'Please enter valid subteam address.';
          }
        }
      },
      {
        name: 'channel',
        type: 'input',
        message: 'Enter the channel:',
        validate: function( value ) {
          if (value.length) {
            return true;
          } else {
            return 'Please enter valid channel.';
          }
        }
      },
      {
        name: 'institute_code',
        type: 'checkbox',
        message: 'Enter your institute_code (The login website subdomain section):',
        choices: [
          'szeszc-deri'
        ],
        default: ['szeszc-deri'],
        validate: function(value) {
          if (value.length) {
            return true;
          } else {
            return 'Please enter valid institute code.';
          }
        }
      },
      {
        name: 'username',
        type: 'password',
        message: 'Enter your Kreta username:',
        validate: function(value) {
          if (value.length) {
            return true;
          } else {
            return 'Please enter your valid username.';
          }
        }
      },
      {
       name: 'password',
       type: 'password',
       message: 'Enter your Kreta password:',
       validate: function(value) {
         if (value.length) {
           return true;
         } else {
           return 'Please enter your valid password.';
         }
       }
     }
    ];
    return inquirer.prompt(questions);
  },
};
