require('dotenv').config(); //For get environment variable
const axios = require('axios');
const User = require("./models/user");
var term = require( 'terminal-kit' ).terminal ;
const inquirer  = require('./input');
const chalk = require('chalk');
const clear = require('clear');
const figlet = require('figlet');
var dayjs = require('dayjs');
var {keybaseMsg,keybaseCreateSubteam, keybaseAddMember, keybaseCreateChannel} = require('./keybase');

const InitiateMongoServer = require("./db");
// Initiate Mongo Server
InitiateMongoServer();


var user_agent;
var mobile_api_url;

var api_idp = '/idp/api/v1/Token';
var api_mapi = '/mapi/api/v1/Student';
var api_timetable = '/mapi/api/v1/Lesson';
var api_key = '7856d350-1fda-45f5-822d-e1a2f3f1acf0';
var api_client_id = '919e0c1c-76a2-4646-a2fb-7085bbbf3c56';
var api_institutes = '/api/v1/Institute';

const getExternalLinks = async () => {
  try {
    var url = await axios.get('http://kretamobile.blob.core.windows.net/configuration/ConfigurationDescriptor.json');
    var agent = await axios.get('https://www.filcnaplo.hu/settings.json');

    user_agent = agent.data.KretaUserAgent;
    mobile_api_url = url.data.GlobalMobileApiUrlPROD;
  } catch (error) {
    console.error(error)
  }
}


async function getInstitute(schoolCheck) {
  let config = {
    headers: {
      apiKey: api_key,
      'User-Agent': user_agent
    }
  }
  var res = await axios.get(mobile_api_url + api_institutes, config);
  var validschool = false;
  res.data.forEach((item, i) => {
    if ((item.InstituteCode).includes(schoolCheck)) {
      validschool = true;
    }
  });
  return validschool;
};

const saveNewValue = async (student, team, subteam, institute_code, username, password, channel) => {
  let config = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'charset': 'utf-8',
      'User-Agent': user_agent
    }
  };
  var api_base_url = 'https://' + institute_code + '.e-kreta.hu';
  var getTokens_payload = 'institute_code=' + institute_code + '&userName=' + username + '&password=' + password + '&grant_type=password&client_id=' + api_client_id;
  var getTokens_url = api_base_url + api_idp;
  var getTokens = await axios.post(getTokens_url, getTokens_payload, config);
  //TODO: Diák neve lekérésé
  /*
  var getPersonalData_payload = 'fromDate=' + dayjs().format('YYYY-MM-DD') + '&toDate=' + dayjs().format('YYYY-MM-DD');
  var getPersonalData_url = api_base_url + api_mapi;
  var getPersonalData = await axios.post(getPersonalData_url, getPersonalData_payload, config);
  */

  //var student = getPersonalData.Name;
  console.log(chalk.green('Valid login!'));

  user = new User({
    student,
    settings: {
      team: team,
      subteam: subteam,
      channel: channel,
      institute_code: institute_code,
      tokens: {
        access_token: getTokens.data.access_token,
        token_type: getTokens.data.token_type,
        expires_at: dayjs().add(1, 'hour').format(),
        refresh_token: getTokens.data.refresh_token
      },
      others: {
        user_agent: user_agent,
        mobile_api_url: mobile_api_url,
        api_base_url: api_base_url
      }
    },
    created_at: dayjs().format()
  });


  await user.save();
  console.log(chalk.green('User saved!'));

  var new_team = team + '.' + subteam;
//  await keybaseCreateSubteam(new_team);
  //await keybaseAddMember(new_team, 'd3v_', 'admin');
//  await keybaseMsg('Sziasztok! Örülök, hogy a csatornára léphettem :-D', team + '.' + subteam);
};

const getInputs = async () => {
  const credentials = await inquirer.askKretaCredetials();
  return credentials;
};

clear();

console.log(
  chalk.yellow(
    figlet.textSync('Kretable-reg', { horizontalLayout: 'full' })
  )
);

getExternalLinks()
  .then(async function() {
    var data = await getInputs();
    await saveNewValue(data.student, data.team[0], data.subteam, data.institute_code[0], data.username, data.password, data.channel);
  });
