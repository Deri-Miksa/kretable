require('dotenv').config(); //For get environment variable
const axios = require('axios');
const keybaseMsg = require('./libs/keybase');
var term = require( 'terminal-kit' ).terminal;
const clear = require('clear');
const figlet = require('figlet');
var dayjs = require('dayjs');

require('./libs/crons');

clear();

console.log( figlet.textSync('Kretable cron service', { horizontalLayout: 'full' }));


const InitiateMongoServer = require("./libs/db");
// Initiate Mongo Server
InitiateMongoServer();
