
const axios = require('axios');

var API_KEY= '7856d350-1fda-45f5-822d-e1a2f3f1acf0';
var API_CLIENT_ID = '919e0c1c-76a2-4646-a2fb-7085bbbf3c56';
var API_INSTITUTES = '/api/v1/Institute';
var API_IDP = '/idp/api/v1/Token';
var API_TIMETABLE = '/mapi/api/v1/Lesson';


exports.getTimetable = async (access_token, user_agent, api_base_url, fromDate, toDate) => {
  try {
    let config = {
      headers: {
        Authorization: 'Bearer ' + access_token,
        'User-Agent': user_agent
      }
    }

    var res = await axios.get(api_base_url + API_TIMETABLE + '?fromDate=' + fromDate + '&toDate=' + toDate, config);
    return res;
  } catch(e){
    console.log(e);
    return 1;
  }
};

exports.getRefresh = async(user_agent, institute_code, refresh_token) => {
  let config = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'charset': 'utf-8',
      'User-Agent': user_agent
    }
  };
  var api_base_url = 'https://' + institute_code + '.e-kreta.hu';
  try {
    var request_payload = 'institute_code=' + institute_code + '&refresh_token=' + refresh_token + '&grant_type=refresh_token&client_id=' + API_CLIENT_ID;
    var request_url = api_base_url + API_IDP;
    var res = await axios.post(request_url, request_payload, config);
    return res;
  } catch (e) {
    console.log(e);
    return 1;
  }

}
