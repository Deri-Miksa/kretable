exports.keybaseMsg = async (team, channel, msg, bot) => {
  try {
    const config = {
      name: team,
      members_type: 'team',
      topic_name: channel
    }
    const message = {
      body: msg,
    }
    await bot.chat.send(config, message)
  } catch (error) {
    console.error("Keybase user notify falled:", error)
  }
}
exports.keybaseDM = async (username, msg, bot) => {
  try {
    const config = {
      name: username,
      topicType: 'chat'
    }
    const message = {
      body: msg,
    }
    await bot.chat.send(config, message)
  } catch (error) {
    console.error("Keybase user notify falled:", error)
  }
}
