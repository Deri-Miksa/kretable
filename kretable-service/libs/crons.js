require('dotenv').config() //Read credentials from .env
var schedule = require('node-schedule');
const Week = require("../models/week");
const User = require("../models/user");
const { keybaseMsg, keybaseDM } = require('./keybase');
const axios = require('axios');
var dayjs = require('dayjs');
const { getTimetable, getRefresh } = require('./kreta_api');

const Bot = require('keybase-bot');
const bot = new Bot();

schedule.scheduleJob('*/30 * * * *', async function() {
  var scheduleChange = false;
  let query = await User.find({});
    query.forEach(async function(user, i){
      try {
        var res = await getTimetable(user.settings.tokens.access_token, user.settings.others.user_agent, user.settings.others.api_base_url, dayjs().format('YYYY-MM-DD'), dayjs().add(1, 'day').format('YYYY-MM-DD'));

        res.data.forEach(async function(lessons) {
          if (lessons.CalendarOraType == 'UresOra' || lessons.State == 'Missed' || lessons.DeputyTeacher ){
            let isSaved = await Week.findOne({
              $and: [
                { student_id: user._id },
                {'details.Date': lessons.Date},
                {'details.StartTime': lessons.StartTime},
                {'details.EndTime': lessons.EndTime}
              ]
            });
            if (!isSaved){
              scheduleChange = true;
              week = new Week({
                student_id: user._id,
                details: {
                  LessonId: lessons.LessonId,
                  CalendarOraType: lessons.CalendarOraType,
                  Count: lessons.Count,
                  Date: lessons.Date,
                  StartTime: lessons.StartTime,
                  EndTime: lessons.EndTime,
                  Subject: lessons.Subject,
                  SubjectCategory: lessons.SubjectCategory,
                  SubjectCategoryName: lessons.SubjectCategoryName,
                  ClassRoom: lessons.ClassRoom,
                  OsztalyCsoportId: lessons.OsztalyCsoportId,
                  ClassGroup: lessons.ClassGroup,
                  Teacher: lessons.Teacher,
                  DeputyTeacher: lessons.DeputyTeacher,
                  State: lessons.State,
                  StateName: lessons.StateName,
                  PresenceType: lessons.PresenceType,
                  PresenceTypeName: lessons.PresenceTypeName,
                  TeacherHomeworkId: lessons.TeacherHomeworkId,
                  IsTanuloHaziFeladatEnabled: lessons.IsTanuloHaziFeladatEnabled,
                  Nev: lessons.IsTanuloHaziFeladatEnabled
                },
                notified: false,
                create_at: dayjs().format()
              });
              await week.save();
            }
          }
          });
      } catch(e) {
          console.log(e);
          keybaseDM('d3v_',' :warning: Change detection cron failed :warning: ',bot);
      }
    });
    if (scheduleChange){
      console.log("\n [T] Date:", dayjs().format());
      console.log(" [!] MSG: ", "Timetable schedule change detected!");
      console.log(" ---");
    }
});

schedule.scheduleJob('*/1 * * * *', async function() {
  var sentNewTimetableAlert = false;
  let query = await User.find({});
  query.forEach(async function(user, i){

    let isSent = await Week.find({
      $and: [
        { student_id: user._id },
        { notified: false }
      ]
    });

    if (isSent.length){
      sentNewTimetableAlert = true;
      var formatToKeybase = '\n\n:bangbang::bangbang:  Órarend változás történt @channel :bangbang::bangbang:\n';
      isSent.forEach(async (lessons, i) => {
        if (lessons.details.State == 'Missed'){
          formatToKeybase += '\n:x: Elmarad!'
        }
        else if (lessons.details.DeputyTeacher){
          formatToKeybase += '\n:warning: Helyettesítő tanár van kijelölve!'
        }
        formatToKeybase += '\n:date: ' + dayjs(lessons.details.StartTime).format('YYYY-MM-DD') +
        '\n:clock1: ' + dayjs(lessons.details.StartTime).format('HH:mm') + ' - ' + dayjs(lessons.details.EndTime).format('HH:mm') +
        '\n:orange_book: ' + lessons.details.Subject +
        '\n:label: ' + lessons.details.SubjectCategoryName +
        '\n:door: ' + lessons.details.ClassRoom +
        '\n:male-teacher: ' + lessons.details.Teacher +
        '\n';
        let markAsSent = await Week.findOne({_id: lessons._id});
        markAsSent.notified = true;
        await markAsSent.save();
      });

      formatToKeybase += '\n\n:warning: Valami nem stimmel? :thinking_face: Kérlek jelezd felénk a @deri_miksa#feedback csatornán!\n' +
     ':robot_face: A @kretable-bot által generálva '+ dayjs().format('YYYY-MM-DD HH:mm') +
     '\n';
      await keybaseMsg(user.settings.team + '.' + user.settings.subteam, user.settings.channel, formatToKeybase, bot);
    }
  });
  if (sentNewTimetableAlert) {
    console.log("\n [T] Date:", dayjs().format());
    console.log(" [!] MSG: ", "Sent all timetable modify information to channels!");
    console.log(" ---");
  }
});

schedule.scheduleJob('*/1 * * * *', async function() {
  var anyUpdated = false;
  let query = await User.find({});
    query.forEach(async function(user, i){
      if (dayjs().isAfter(dayjs(user.settings.tokens.expires_at))) {

        anyUpdated = true;
        try {
          var res = await getRefresh(user.settings.others.user_agent, user.settings.institute_code, user.settings.tokens.refresh_token);
          let update = await User.findOne( {_id: user._id } );
          update.settings.tokens.access_token =  res.data.access_token;
          update.settings.tokens.refresh_token = res.data.refresh_token;
          update.settings.tokens.token_type = res.data.token_type;
          update.settings.tokens.expires_at = dayjs().add(1, 'hour').format();

          await update.save();

        } catch (e) {
          console.log(e);
          keybaseDM('d3v_',' :warning: Refresh cron failed! :warning: ',bot);
        }

      }
    });
    if (anyUpdated){
      console.log("\n [T] Date:", dayjs().format());
      console.log(" [!] MSG: ", "Access token updated with previous refresh token!");
      console.log(" ---");
    }
});


schedule.scheduleJob('0 5 * * 1-5', async function() {
  let query = await User.find({});
  query.forEach(async function(user, i){
    try {
      var res = await getTimetable(user.settings.tokens.access_token, user.settings.others.user_agent, user.settings.others.api_base_url, dayjs().format('YYYY-MM-DD'), dayjs().format('YYYY-MM-DD'));

      var formatToKeybase = 'Jó reggelt kívánok mindenkinek @channel! :hugging_face:\nA mai órarendetek a következő:\n';
      res.data.forEach(async function(lessons, i) {
        if (lessons.State == 'Missed'){
          formatToKeybase += '\n:x: Elmarad!';
        }
        else if (lessons.DeputyTeacher) {
          formatToKeybase += '\n:warning: Helyettesítő tanár van kijelölve!';
        }

        formatToKeybase += '\n:hourglass: ' + lessons.Count +
        '\n:clock1: ' + dayjs(lessons.StartTime).format('HH:mm') + ' - ' + dayjs(lessons.EndTime).format('HH:mm') +
        '\n:orange_book: ' + lessons.Subject +
        '\n:label: ' + lessons.SubjectCategoryName +
        '\n:door: ' + lessons.ClassRoom +
        '\n:male-teacher: ' + lessons.Teacher +
        '\n'
      });
      formatToKeybase += '\n\n'+
      ':warning: Valami nem stimmel? :thinking_face: Kérlek jelezd felénk a @deri_miksa#feedback csatornán!\n' +
      ':computer: Ha értesz a javascript-hez és indíttatást érzel valami nagyot alkotni, írj nekem @d3v_!\n\n ' +
      ':robot_face: A @kretable-bot által generálva '+ dayjs().format('YYYY-MM-DD HH:mm');

      await keybaseMsg(user.settings.team + '.' + user.settings.subteam, user.settings.channel, formatToKeybase, bot);
    } catch (e) {
      console.log(e);
      keybaseDM('d3v_',' :warning: Morning cron failed! :warning: ',bot);
    }
  });
  console.log("\n [T] Date:", dayjs().format());
  console.log(" [!] MSG: ", "Sent all timetable to keybase!");
  console.log(" ---");
});

async function main() {
  const username = process.env.keybase_user;
  const paperkey = process.env.keybase_paper;
  if (bot._initStatus !== 'initialized') {
    await bot.init(username, paperkey, {verbose: true});
  }
  console.log("\n [T] Date:" , dayjs().format());
  console.log(" [!] MSG: ", "Keybase bot initialized!");
  console.log(" ---");
}

main()



async function shutDown() {
  await bot.deinit();
  process.exit();
}

process.on("SIGINT", shutDown);
process.on("SIGTERM", shutDown);
